%undefine __cmake_in_source_build
%global sover 0.2

Name:           lpcnetfreedv
Version:        0.2
Release:        1
Summary:        LPCNet for FreeDV

License:        BSD
URL:            https://github.com/drowe67/LPCNet
Source0:        https://github.com/drowe67/LPCNet/archive/v%{version}/LPCNet-%{version}.tar.gz
Source1:        http://rowetel.com/downloads/deep/lpcnet_191005_v1.0.tgz

BuildRequires:  cmake gcc
BuildRequires:  codec2-devel

%description
Experimental version of LPCNet that has been used to develop FreeDV 2020 - a HF
radio Digial Voice mode for over the air experimentation with Neural Net speech
coding. Possibly the first use of Neural Net speech coding in real world
operation.

%package devel
Requires:       %{name}%{?_isa} = %{version}-%{release}
Summary:        Development files and tools for LPCNet

%description devel
%{summary}.


%prep
%autosetup -p1 -n LPCNet-%{version}


%build
# Add model data archive to the build directory so CMake finds it.
	
mkdir -p %{_vpath_builddir}
cp %{SOURCE1} %{_vpath_builddir}/
 
# We need to force optimizations to specific values since the build system and
# host system will likely be different.
%ifarch i686 x86_64
    %global _cpuopt "-DAVX=TRUE"
%endif
%ifarch armv7hl
    %global _cpuopt "-DNEON=TRUE"
%endif
%ifarch aarch64 ppc64le s390x riscv64
    # NEON instructions are native in arm64.
    %global _cpuopt ""
%endif

cd %{_vpath_builddir}
/usr/bin/cmake \
-DCMAKE_C_FLAGS_RELEASE:STRING=-DNDEBUG \
-DCMAKE_CXX_FLAGS_RELEASE:STRING=-DNDEBUG \
-DCMAKE_Fortran_FLAGS_RELEASE:STRING=-DNDEBUG \
-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
-DCMAKE_INSTALL_PREFIX:PATH=/usr \
-DINCLUDE_INSTALL_DIR:PATH=/usr/include \
-DLIB_INSTALL_DIR:PATH=/usr/lib64 \
-DSYSCONF_INSTALL_DIR:PATH=/etc \
-DSHARE_INSTALL_PREFIX:PATH=/usr/share \
-DLIB_SUFFIX=64 \
-DBUILD_SHARED_LIBS:BOOL=ON \
-DDISABLE_CPU_OPTIMIZATION=TRUE \
..

make %{?_smp_mflags}

%install
%cmake_install


%check
# Test scripts incorrectly assume build directory name. Need to fix.
#ctest


%files
%license COPYING
%doc README.md
%{_libdir}/lib%{name}.so.%{sover}

%files devel
%{_bindir}/*
%{_includedir}/lpcnet/
%{_libdir}/cmake/lpcnetfreedv/
%{_libdir}/lib%{name}.so


%changelog
* Thu Apr 18 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 0.2-1
- init package
